window.onload = () => {

    //Работа с контекстом(bind call apply)
    /////////////////////////
    const userOne = {
        firstName: "Petro",
        lastName: "Maga",
        age: "35",
        user() {
            console.log(`Привет! Я ${this.firstName} ${this.lastName}`);
        }
    }
    userOne.user();

    let userTwo = {
        firstName: "Quentin",
        lastName: "Tarantino",
        age: "50",
    }
    userOne.user.apply(userTwo);

    //Создайте метод который будет умножать элементы массива 
    //на то число которое будет передавать пользователь. 
    //Сделайте так, чтобы метод наследовался каждым массивом 
    //подобно методу pop().
    ///////////////////////////////////

    let arr = [25, 37, 47, 54, 31];

    let a = prompt("введите число");

    const mnoj = function (array) {
        let newArr = [];
        array.forEach(e => {
            let newValue = a * e;
            newArr.push(newValue);
        });
        console.log(newArr);
    }
    mnoj(arr);

    let arr2 = [11, 15, 37, 12, 4];

    mnoj(arr2);

    //Сделайте функцию, которая считает и 
    //выводит количество своих вызовов.
    ///////////////////////////
   const func = function () {
        let i = 1;
        return () => ("Функция вызвана: " + (i++) + " раз");
    }
    let count = func();

    console.log(count());
    console.log(count());
    console.log(count());
    console.log(count());
    //////////////////////////

    //Сделайте функцию, каждый вызов который будет 
    //генерировать случайные числа от 1 до 100, но так, 
    //чтобы они не повторялись, пока не будут перебраны 
    //все числа из этого промежутка. Решите задачу через 
    //замыкания - в замыкании должен хранится массив чисел, 
    //которые уже были сгенерированы функцией.
    /////////////////////////
    const generator100 = function () {
        let arr100 = [];
        let rand = function () {
            return Math.floor((Math.random() * 100) + 1);
        }
        let perebor = function (arr, e) {
            for (let i = 0; i < arr.length; i++) {
                if (arr[i] == e) {
                    return e;
                }
            }
        }
        let perebor2 = function () {
            for (let i = 0; i < 1000; i++) {
                let r = rand();
                if (perebor(arr100, r) == r) {
                    false;
                } else {
                    arr100.push(r);
                    if (arr100.length == 100) {
                        return;
                    }
                }
            }
        }
        perebor2();
        console.log(arr100);
    };
    generator100();
}
